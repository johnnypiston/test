# Тестовое задание

## Команды для запуска проекта:

- **npm install** - установка зависимостей;
- **npm start** - запуск проекта в режиме для разработки;
- **npm run build** - сборка проекта для продакшена в папку build.

**Описание:** Первоначальная настройка проекта с помощью Create React App.
Конечно, для получения данных и проверки результата нужно было настроить свой локальный сервер. Но я не имел подобного опыта. Потому решил сосредоточиться на других задачах. А json с вопросами и ответами запрашивать с github.

---

**Задача:** разработать одностраниченое веб-приложение (SPA) с возможностью пройти тестирование и выводом последующих результатов теста

**Требования:**

1. Использование VueJs
2. Запрещено использовать jQuery
3. Загрузка вопросов должна происходить асинхронно через http запрос к json файлу
4. Оптимизация для просмотра с мобильного устройства
5. Кросс-браузерная верстка

Структура json с вопросами остается на усмотрение исполнителя

**Плюсом будет если:**

1. Запрос к json файлу кешируется в локальное хранилище
2. Вопросы с несколькими вариантами ответов
3. Отображение статистики попыток прохождения тестов
4. Более 1 теста и меню для перехода между ними
5. Иллюстрации к вопросам и оптимизация их загрузки
6. На странице теста отрисован один вопрос, переход к остальным осуществляется через пагинацию

**Ожидаемый результат:**
Возможность пройти тестирование и увидеть результат

# Test task

**Objective:** to develop a single-page web application (SPA) with the ability to pass a test and display test results

**Requirements:**

1. Using VueJs
2. It is forbidden to use jQuery
3. Loading questions should occur asynchronously via http request to json file
4. Optimized for viewing from a mobile device
5. Cross Browser Layout

The json structure with questions remains at the discretion of the performer.

**A plus would be if:**

1. Request to json file is cached in local storage
2. Multiple-choice questions
3. Display statistics of attempts to pass tests
4. More than 1 test and menu to switch between them
5. Illustrations for questions and optimization of their loading
6. On the test page, one question is drawn, the transition to the rest is done through pagination.

**Expected Result:**
Ability to be tested and see the result
