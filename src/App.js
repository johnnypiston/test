import React from 'react';
import 'es6-promise/auto';
import { Base64 } from 'js-base64';
import axios from 'axios';
import Question from './question/Question';
import PaginationControls from './controls/PaginationControls';
import ResultMessage from './results/ResultMessage';
import ResultControls from './controls/ResultControls';
import ResultAnswersList from './results/ResultAnswersList';
import './buttons.css';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      questions: [],
      answers: {},
      currentQuestion: 1,
      isNextBtnActive: false,
      isResultShow: false,
      result: 0,
      isLoading: true,
      isShowAnswers: false,
    };

    this.handleCheckTest = this.handleCheckTest.bind(this);
    this.handleAnswerChange = this.handleAnswerChange.bind(this);
    this.handleShowNext = this.handleShowNext.bind(this);
    this.handleShowPrev = this.handleShowPrev.bind(this);
    this.handleStartAgain = this.handleStartAgain.bind(this);
    this.handleShowAnswers = this.handleShowAnswers.bind(this);
  }

  componentDidMount() {
    const url =
      'https://api.github.com/repos/johnnypiston/questions_for_test/contents/questions.json';

    axios
      .get(url)
      .then(response => {
        let questions;
        questions = JSON.parse(Base64.decode(response.data.content));
        this.setState({
          testName: questions.category,
          questions: questions.questions,
          isLoading: false,
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  handleCheckTest(e) {
    e.preventDefault();
    const answers = this.state.answers;
    const questions = this.state.questions;
    let result = this.state.result;

    questions.forEach(function(question, i) {
      let questionName = question.id;
      let correctAnswer = question.correct_answer;
      if (answers[questionName] === correctAnswer) {
        result += 1;
      }
    });
    this.setState({
      isResultShow: true,
      result: result,
    });
  }

  handleAnswerChange(question, answerValue) {
    let answers = this.state.answers;
    answers[question] = answerValue;
    this.setState(state => ({
      answers: answers,
      isNextBtnActive: state.currentQuestion < state.questions.length,
    }));
  }

  handleShowNext(e) {
    e.preventDefault();
    this.setState(state => ({
      currentQuestion: state.currentQuestion + 1,
      isNextBtnActive: false,
    }));
  }

  handleShowPrev(e) {
    e.preventDefault();
    this.setState(state => ({
      currentQuestion: state.currentQuestion - 1,
      isNextBtnActive: true,
    }));
  }

  handleStartAgain(e) {
    e.preventDefault();
    this.setState({
      answers: {},
      currentQuestion: 1,
      isNextBtnActive: false,
      isResultShow: false,
      result: 0,
      isShowAnswers: false,
    });
  }

  handleShowAnswers(e) {
    e.preventDefault();
    this.setState({
      isShowAnswers: true,
    });
  }

  render() {
    const isResultShow = this.state.isResultShow;
    const questions = this.state.questions;
    const currentQuestion = this.state.currentQuestion;
    const questionToRender = questions[currentQuestion - 1];
    const answersCount = Object.keys(this.state.answers).length;

    if (this.state.isLoading)
      return (
        <div className="layout">
          <div className="test">
            <div className="loading" />
          </div>
        </div>
      );
    if (!this.state.isLoading)
      return (
        <>
          <main>
            <div className="layout">
              <div className="test">
                <h1 className="test__title">{this.state.testName}</h1>
                {!isResultShow ? (
                  <form className="test__form" onSubmit={this.handleCheckTest}>
                    <Question
                      question={questionToRender}
                      questionNumber={this.state.currentQuestion}
                      onAnswerChange={this.handleAnswerChange}
                      answers={this.state.answers}
                    />
                    <PaginationControls
                      currentQuestion={currentQuestion}
                      questionsCount={questions.length}
                      answersCount={answersCount}
                      nextBtnStatus={this.state.isNextBtnActive}
                      handleShowNext={this.handleShowNext}
                      handleShowPrev={this.handleShowPrev}
                    />
                  </form>
                ) : (
                  <>
                    <ResultMessage
                      result={this.state.result}
                      questionsCount={questions.length}
                    />
                    <ResultControls
                      handleShowAnswers={this.handleShowAnswers}
                      handleStartAgain={this.handleStartAgain}
                    />
                  </>
                )}
              </div>

              {this.state.isShowAnswers && (
                <ResultAnswersList
                  answers={this.state.answers}
                  questions={this.state.questions}
                />
              )}
            </div>
          </main>
        </>
      );
  }
}

export default App;
