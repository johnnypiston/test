import React from 'react';
import './controls.css';

const PaginationControls = function(props) {
  let nextStepBtn;

  const handleShowNext = function(e) {
    props.handleShowNext(e);
  };

  const handleShowPrev = function(e) {
    props.handleShowPrev(e);
  };

  if (props.currentQuestion < props.questionsCount) {
    nextStepBtn = (
      <button
        className="btn btn--green next-btn"
        type="button"
        onClick={handleShowNext}
        disabled={!props.nextBtnStatus}
      >
        Вперед
      </button>
    );
  } else {
    nextStepBtn = (
      <button
        className="btn btn--green next-btn"
        type="submit"
        disabled={!(props.answersCount === props.questionsCount)}
      >
        Проверить
      </button>
    );
  }

  return (
    <div className="controls">
      {props.currentQuestion > 1 && (
        <button
          className="btn btn--transparent"
          type="button"
          onClick={handleShowPrev}
        >
          Назад
        </button>
      )}
      {nextStepBtn}
    </div>
  );
};

export default PaginationControls;
