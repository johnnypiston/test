import React from 'react';
import './controls.css';

const ResultControls = function(props) {
  const handleStartAgain = function(e) {
    props.handleStartAgain(e);
  };
  const handleShowAnswers = function(e) {
    props.handleShowAnswers(e);
  };

  return (
    <div className="controls">
      <button
        className="btn btn--transparent"
        type="button"
        onClick={handleShowAnswers}
      >
        Показать ответы
      </button>
      <button
        className="btn btn--green next-btn"
        type="button"
        onClick={handleStartAgain}
      >
        Пройти тест заново
      </button>
    </div>
  );
};

export default ResultControls;
