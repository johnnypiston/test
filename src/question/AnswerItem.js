import React from 'react';
import './customRadio.css';

const AnswerItem = function(props) {
  const handleChange = function(e) {
    const question = e.target.name;
    const answerValue = e.target.value;
    props.onAnswerChange(question, answerValue);
  };

  return (
    <label className="custom-radio">
      <input
        type="radio"
        name={props.questionId}
        value={props.answer}
        checked={props.answers[props.questionId] === props.answer}
        onChange={handleChange}
      />
      <span className="custom-radio__indicator" />
      {props.answer}
    </label>
  );
};

export default AnswerItem;
