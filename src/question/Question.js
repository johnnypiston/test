import React from 'react';
import AnswerItem from './AnswerItem';
import './question.css';

const Question = function(props) {
  const answersList = props.question.answers.map((answer, itemNum) => (
    <li key={itemNum.toString()}>
      <AnswerItem
        answer={answer}
        questionId={props.question.id}
        onAnswerChange={props.onAnswerChange}
        answers={props.answers}
      />
    </li>
  ));

  return (
    <div className="question">
      <p className="question__text">
        <span className="question__number">{props.questionNumber} из 10</span>{' '}
        {props.question.question_text}
      </p>
      <ul className="question__answers-list">{answersList}</ul>
    </div>
  );
};

export default Question;
