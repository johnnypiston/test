import React from 'react';
import './answers-list.css';

const ResultAnswersList = function(props) {
  const answers = Object.keys(props.answers).map(
    answerKey => props.answers[answerKey]
  );
  const answersList = answers.map((item, i) => (
    <li
      className={
        'answers-list__item answers-list__item' +
        (props.questions[i].correct_answer === item ? '--correct' : '--wrong')
      }
      key={i.toString()}
    >
      <p className="answers-list__question">
        {i + 1 + '. ' + props.questions[i].question_text}
      </p>
      <p>{item}</p>
    </li>
  ));
  return <ul className="answers-list">{answersList}</ul>;
};

export default ResultAnswersList;
