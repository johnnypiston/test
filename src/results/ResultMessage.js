import React from 'react';
import './result-text.css';

const ResultMessage = function(props) {
  return (
    <div className="result-text">
      {'Правильных ответов: '}
      <span>{props.result}</span>
      {' из ' + props.questionsCount}
    </div>
  );
};

export default ResultMessage;
